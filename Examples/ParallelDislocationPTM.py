#!/usr/bin/env python

from numpy import *
from asap3 import *
from asap3.analysis import CNA
from ase.lattice.cubic import FaceCenteredCubic
from asap3.setup.dislocation import Dislocation
from asap3.md.verlet import VelocityVerlet
from asap3.analysis.ptm import PTMdislocations
from ase.visualize.primiplotter import *
from ase.io import write
from ase import units
from ase.parallel import world

print_version(1)

splitting = 5
size = (50, 88, 35)
#size = (30, 25, 7)

Gold = "Au"
cpulayout = (1, 1, world.size)

if world.rank == 0:
    slab = FaceCenteredCubic(directions=((1,1,-2), (-1,1,0), (1,1,1)),
                            size=size, symbol=Gold, pbc=False)
    basis = slab.get_cell()
    print basis
    print "Number of atoms:", len(slab)
    
    center = 0.5 * array([basis[0,0], basis[1,1], basis[2,2]]) + array([0.1, 0.1, 0.1])
    offset = 0.5 * splitting * slab.miller_to_direction((-1,0,1))
    print center

    d1 = Dislocation(center - offset, slab.miller_to_direction((-1,-1,0)),
                    slab.miller_to_direction((-2,-1,1))/6.0)
    d2 = Dislocation(center + offset, slab.miller_to_direction((1,1,0)),
                    slab.miller_to_direction((1,2,1))/6.0)
    atoms = Atoms(slab)
    (d1+d2).apply_to(atoms)
    del slab
    atoms = MakeParallelAtoms(atoms, cpulayout)
else:
    atoms = MakeParallelAtoms(None, cpulayout)
    
atoms.set_calculator(EMT(EMTRasmussenParameters()))
dyn = VelocityVerlet(atoms, 5*units.fs, logfile='-')
ptm = PTMdislocations(atoms, cutoff=4.5, target_structures=('fcc', 'hcp'),
                      analyze_first=False)
dyn.attach(ptm.analyze, interval=5)

def invis1(atoms):
    y = array(atoms.get_positions()[:,1])
    invis = equal(atoms.get_tags(), 1) + less(y, 5) + greater(y, atoms.get_cell()[1,1] - 5)
    return invis

def invis2(atoms):
    z = array(atoms.get_positions()[:,2])
    invis = equal(atoms.get_tags(), 1) + less(z, 5) + greater(z, atoms.get_cell()[2,2] - 5)
    return invis

p1 = ParallelPrimiPlotter(atoms)
p1.set_rotation([-88,0,0])
p1.set_invisibility_function(invis1)

p2 = ParallelPrimiPlotter(atoms)
p2.set_invisibility_function(invis2)

for i, p in enumerate((p1,p2)):
    p.set_colors({0:"blue", 1:"white", 2:"green", 6:"orange", 7:"yellow", 8:"red"})
    p.set_output(X11Window())
    p.set_output(PngFile("init{0}_".format(i)))
    ptm.attach(p.plot)

dyn.run(5)




