#PBS -l nodes=1:ppn=4:opteron4
#PBS -q verylong
#PBS -N amc_Tfict400_n100_end
#PBS -m ae
#ASAP S
"""Ends atom simulation and gives result in file.
Usage:
       asap-qsub endatomsimulation.py amcfolder e_filter T_real result_log_file
Where: e_filter is the maximal accepted energy w.r.t. the global min such that a cfg. counts.
       T_real is the real gas temperature in K
       result_log_file is the desired name of result file. 
"""
from __future__ import print_function
import sys
import os
from asap3.nanoparticle_mc.atommontecarlodata import AtomMonteCarloData
import numpy as np
from ase import units #used to get kB in eV/K
from asap3.nanoparticle_mc.mc_result import *
#Check if user asked for help with -h or --help or -doc
for a in sys.argv:
    if a=="-h" or a == "--help" or a == "-doc" or a == "--documentation":
        print(__doc__, file=sys.stderr)
        sys.exit(0)

fdir = sys.argv[1] #Directory of .amc files to join
e_filter = float(sys.argv[2])
T = float(sys.argv[3]) #The real temperature e.g. 300 K 
logfile = str(sys.argv[4])

out = open(logfile,"w",1)
print("Atom Monte Carlo Simulation Summary \n -------------------------------------------", file=out)



files = []
os.chdir(fdir)

for f in os.listdir("./"):
    if f.endswith(".amc.gz"):
        files.append(f)
#Now we know the file names. Read merge the informations.
d=AtomMonteCarloData()
dtemp = AtomMonteCarloData()
#Init arrays
relaxed_energies = np.array([])
unrelaxed_energies = np.array([])
mc_energies = np.array([])
real_energies = np.array([])
counts = np.array([])
#types = np.array([])
coordinations = None
vac_coordinations=None
#vac_types = np.array([])
multiplicities = np.array([], int)
mc_eglobalmin=None
eglobalmin_id = None
real_eglobalmin = None
ninsane = 0
mc_globalmin_file = None #globalminfile
real_globalmin_file = None

def delmultiple(listtohandle,idstorem):
    offset = 0
    for i in idstorem:
        del listtohandle[i-offset]
        offset+=1
    return listtohandle

for f in files: #Now cut half the MC steps off, to only consider Thermal equilibrium.
   #At the same time find the global min. energy
    try:
        dtemp.read(f) #loads pickle to dtemp!
    except:
        err_out = open("error_amc_end","w",1)
        err_out.write("Error in file"+str(f))
        err_out.close()
        sys.exit(1)
        
    ninsane += dtemp.ninsane
    id_r = dtemp.id_relax
    if id_r==None:
        print(f)
    relaxed_energies = np.append(relaxed_energies,dtemp.lists['relaxed_energies'][id_r:])
    
    unrelaxed_energies = np.append(unrelaxed_energies,dtemp.lists['unrelaxed_energies'][id_r:])
    
    mc_energies = np.append(mc_energies,dtemp.lists['energy'][id_r:])
    
    #Calculate the real energies:
    r2append = np.array(dtemp.lists['energy'][id_r:]) + np.array(dtemp.lists['relaxed_energies'][id_r:])-np.array(dtemp.lists['unrelaxed_energies'][id_r:])
    
    real_energies = np.append(real_energies,r2append)

    
    
    counts = np.append(counts,dtemp.lists['counts'][id_r:])

    nn = len(dtemp.lists['relaxed_energies'])
    multiplicities = np.append(multiplicities, dtemp.multiplicity * np.ones(nn, int)[id_r:])
    
    #types =  np.append(types,dtemp.lists['types'][id_r:])
    #print dtemp.lists['coordinations'][int((len(dtemp.lists['coordinations'])-1)/2):]
    #print "ID Relax " + str(id_r)
    coord = np.array(dtemp.lists['coordinations'][id_r:], np.int32)
    
    if coordinations is None:
        coordinations = np.zeros((0, len(coord[0])), np.int32) 
        
        
    coordinations = np.vstack([coordinations, coord])
    with open("../progress.txt", "w", 1) as status:
        status.write(f + '\n')
        status.write("coord shape: {0}\n".format(str(np.array(coord).shape)))
        status.write("coordinations shape: {0}  itemsize={1}\n".format(str(coordinations.shape), coordinations.itemsize))

    
    del coord
    
    emin_real = real_energies.min() #to find real min.
    #If lower energy is found
    emin = mc_energies.min()
    if mc_eglobalmin is None or emin<mc_eglobalmin:
        mc_eglobalmin = emin #The new global min is stored.
        eglobalmin_id = mc_energies.argmin()
        mc_globalmin_file = f 
    
    if real_eglobalmin is None or emin_real < real_eglobalmin:
        real_eglobalmin = emin_real
        real_eglobalmin_id = real_energies.argmin()
        real_globalmin_file = f  
    #Store temperature:
    Tsim = dtemp.temp
    
    #Store latticeconstant:
    lc = dtemp.lattice_constant
    if lc is None:
        lc = 4.055 #The Au fcc-EMT lattice constant

print("Found Global Min MC " +str(mc_eglobalmin)+ "eV" + "in file", mc_globalmin_file, file=out)
print("Current global Min of real energies", real_eglobalmin, "in file", real_globalmin_file, file=out)
#print >> out, "Global min lies in file: "+str(eglobalmin_file)+" with index "+str(eglobalmin_id)
print("\n ---------------------------------\n", file=out)

print("Tsim = "+str(Tsim), file=out)
print("Tcorrect = "+str(T), file=out)
print("E_filter = "+str(e_filter), file=out)       
#Now we know which file contains the min.en config and which energy aswell as the id in
# the combined array(energies)
# Use the big arrays to filter out.
#badidx = [] #Store the non-relevant configurations ids.
#for i in range(len(real_energies)):
#    if mc_energies[i] > mc_eglobalmin+e_filter:
#        badidx.append(i)
goodmask = mc_energies <= mc_eglobalmin+e_filter

print("counts min,max,shape:", counts.min(), counts.max(), counts.shape, file=out)
    
#Now remove these indices from the arrays
relaxed_energies = relaxed_energies[goodmask]
unrelaxed_energies = unrelaxed_energies[goodmask]
mc_energies = mc_energies[goodmask]
real_energies = real_energies[goodmask]
counts = counts[goodmask]
multiplicities = multiplicities[goodmask]
#types = np.delete(types,badidx)
coordinations = coordinations[goodmask]
#vac_coordinations = delmultiple(vac_coordinations,badidx)
#vac_types = np.delete(vac_types,badidx)
print("Counting configurations" + str(len(counts)), file=out)
print("Found "+str(goodmask.sum())+ " relevant configurations \n ---------------------------------\n", file=out)
print(ninsane, " configurations relaxed too far away", file=out)
print("counts min,max,shape:", counts.min(), counts.max(), counts.shape, file=out)



#Now compute Boltzmann averages with the modified BZ-factor
p = counts*multiplicities*np.exp(-(real_energies-real_energies.min())/(units.kB*T)+
                 (mc_energies-mc_energies.min())/(units.kB*Tsim))
                   


#Compute the normalization factor:
factnorm = float(p.sum())


#Normalize the weights.
if not factnorm==0:
    p = p/factnorm
print("Sum of weights: "+str(p.sum())+"\n --------------------------------- \n", file=out)




#Compute Bz averages:
emean = (real_energies*p).sum()
#Make np.array hold the bz average of coordination 0 on [0] and 1 on [1]...
cormean = [(coordinations[:,i]*p).sum() for i in range(13)]
ix=0
for cc in cormean:
    print("Found average "+str(ix)+"-coordinated atoms: "+str(cc), file=out)
    ix+=1
print("\n --------------------------------- \n", file=out)
print("Sum of average coordinated atoms counted: "+str(sum(cormean)), file=out)

print("number of atoms: "+str(coordinations[0].sum()), file=out)

print("Found mean energy "+str(emean)+" eV", file=out)
natoms = float(coordinations[0].sum())
nunitcells = natoms/4.0
diam = lc*(6*nunitcells/np.pi)**(1.0/3)

print("Particle Diameter Under Spherical assumption: "+str(diam) + " Ang", file=out)

out.close()

#Find 1 coordinated:
ix = 0


#Dump result to pickle here an example of vacuum, if gas you could put in Tgas,Pgas:
#filename=None makes the result determine filename upon pickling.

#res = MC_Result(av_coordi = cormean,av_energy=emean,tgas=T,tamc=Tsim,natoms=natoms,diameter=diam)
#res.PickleIt()
